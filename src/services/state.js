import Vue from 'vue'
const state = Vue.observable({
    token: localStorage.getItem('token') || "",
    isLogin: false,
    model: null
})

export default state;