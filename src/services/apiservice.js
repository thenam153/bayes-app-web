import axios from 'axios'
import Stage from './state'
import Config from '../config/default.json'
class ApiService {

    constructor(baseUrl) {
        // super();
        // this.baseUrl = window.localStorage.getItem('BASE_URL')
        this.baseUrl = (baseUrl || Config.BASE_URL) + "/api"
    }
    setBaseUrl(baseUrl) {
        this.baseUrl = (baseUrl || Config.BASE_URL) + "/api"
    }
    getToken() {
        return Stage.token || ""
    }
    httpPost(uri, payload = {}, headers = {"Access-Control-Allow-Origin": "*"}) {
        headers.token = this.getToken()

        return new Promise((resolve, reject) => {
            axios.post(this.baseUrl + uri, payload, { headers })
            .then(res => {
                resolve(res.data)
            })
            .catch(err => {
                reject(err)
            })
        })
    }
    login(payload) {
        
    }
    logout(payload) {
        
    }
    changePassword(payload) {
        return this.httpPost("/user/change-password", payload)
    }
    getUserInfo(payload = {}) {
        return this.httpPost("/user/get-info", payload)
    }

    getModelByKey(payload = {}) {
        return this.httpPost("/model/get-by-key", payload)
    }

    getModelById(payload = {}) {
        return this.httpPost("/model/get-by-id", payload)
    }

    getListModel(payload = {}) {
        return this.httpPost("/model/get-list", payload)
    }

    createModel(payload = {}) {
        return this.httpPost("/model/create", payload)
    }

    updateModel(payload = {}) {
        return this.httpPost("/model/update", payload)
    }

    deleteModel(payload = {}) {
        return this.httpPost("/model/delete", payload)
    }

    refreshToken(payload = {}) {
        return this.httpPost("/model/refresh-token", payload)
    }

    put(payload = {}) {
        return this.httpPost("/model/put", payload)
    }

    train(payload = {}) {
        return this.httpPost("/model/train", payload)
    }

    predict(payload = {}) {
        return this.httpPost("/model/predict", payload)
    }

    getListModelAll(payload = {}) {
        return this.httpPost("/model/get-list-all", payload)
    }

    getListModelPublic(payload = {}) {
        return this.httpPost("/model/get-list-public", payload)
    }

    setStatusModel(payload = {}) {
        return this.httpPost("/model/set-status", payload)
    }
    getTagOfModel(payload = {}) {
        return this.httpPost("/model/get-tags", payload)
    }

}

let apiService = new ApiService()
export default apiService